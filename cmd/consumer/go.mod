module bitbucket.org/newyuinc/ttosi-nsq/cmd/consumer

go 1.21.0

require (
	github.com/golang/snappy v0.0.1 // indirect
	github.com/nsqio/go-nsq v1.1.0 // indirect
)
