package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	nsq "github.com/nsqio/go-nsq"
)

type myMessageHandler struct{}

func (h *myMessageHandler) HandleMessage(m *nsq.Message) error {
	if len(m.Body) == 0 {
		return fmt.Errorf("blank body")
	}
	fmt.Printf("Handler 1: msg: %s\n", string(m.Body))

	return nil
}

type secondMessageHandler struct{
	numProcessed int
	numTotal int
	topic string
	p *nsq.Producer
	c *nsq.Consumer
}

func (h *secondMessageHandler) HandleMessage(m *nsq.Message) error {
	if len(m.Body) == 0 {
		return fmt.Errorf("blank body")
	}
	// fmt.Printf("Handler 2: msg: %s\n", string(m.Body))
	h.numProcessed += 1
	
	// fmt.Printf("processed %s\n", string(h.numProcessed))
	err := h.p.Publish(h.topic, []byte(m.Body))
	if err != nil {
		return err
	}
	if h.numProcessed == h.numTotal {
		// time.Sleep(3 * time.Second) 
		h.c.Stop()
	}
	return nil
}

func main() {
	nsqSource := os.Args[1]
	nsqDest := os.Args[2]
	topic := os.Args[3]
	channel := os.Args[4]
	numTotal, err := strconv.Atoi(os.Args[5])
	if err != nil {
		fmt.Printf("error")
	}
	
	config := nsq.NewConfig()
	consumer, err := nsq.NewConsumer(topic, channel, config)
	if err != nil {
		log.Fatal(err)
	}
	
	
	
	producer, err := nsq.NewProducer(nsqDest, config)
	if err != nil {
		fmt.Printf("error")
	}
	
	defer producer.Stop()
	defer consumer.Stop()
	
	consumer.AddHandler(&secondMessageHandler{numProcessed: 0, numTotal: numTotal, topic: topic, p: producer, c: consumer})
	consumer.ChangeMaxInFlight(1)
	consumer.ConnectToNSQD(nsqSource)
	// err = consumer.ConnectToNSQLookupd("localhost:8084")
	if err != nil {
		log.Fatal(err)
	}

	shutdown := make(chan os.Signal, 2)
	signal.Notify(shutdown, syscall.SIGINT)

	for {
		select {
		case <-consumer.StopChan:
			return
		case <-shutdown:
			fmt.Printf("stopping")
			consumer.Stop()
		}
	}

}
