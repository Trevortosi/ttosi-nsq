package main

import (
	"log"
	"os"
	"time"

	"github.com/google/uuid"
	nsq "github.com/nsqio/go-nsq"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Flags = initializeFlags()

	app.Action = produceMessages

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func produceMessages(c *cli.Context) error {
	config := nsq.NewConfig()
	producer, err := nsq.NewProducer("phoenix-nsq:4150", config)
	if err != nil {
		return err
	}
	defer producer.Stop()

	n := c.Int("num-messages")
	t := c.String("topic")
	m := c.String("message")
	d := c.String("defer")

	useCustom := m != ""

	duration, err := time.ParseDuration(d)
	if err != nil {
		return err
	}

	for i := 0; i < n; i++ {
		if !useCustom {
			m = uuid.New().String()
		}

		err = producer.DeferredPublish(t, duration, []byte(m))
		if err != nil {
			return err
		}
	}

	return nil
}

func initializeFlags() (flags []cli.Flag) {
	flags = append(flags, &cli.IntFlag{
		Name:  "num-messages",
		Value: 1,
		Usage: "Number of messages to send.  Default is 1",
	})

	flags = append(flags, &cli.StringFlag{
		Name:  "topic",
		Value: "ttosiTest",
		Usage: "topic of the message to send",
	})

	flags = append(flags, &cli.StringFlag{
		Name:  "message",
		Usage: "Optional message to use.  If not specified, will generate random messages",
	})

	flags = append(flags, &cli.StringFlag{
		Name:  "defer",
		Usage: "Queue messages at the channel level for a specified time",
		Value: "0s",
	})

	return flags
}
